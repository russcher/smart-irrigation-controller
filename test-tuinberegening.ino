#include "test.hpp"
const int pump_top_pin = 13;
const int pump_bottom_pin = 12;
const int led_error_pin = 11;
const int button_pin = 2;
const int water_sensor_top_pin = 3;
const int water_sensor_bottom_pin = 4;
int tank_status = 0;
// tank_status 0 = empty
// tank_status 1 = filling
// tank_status 2 = full
// tank_status 3 = emptying
// tank_status can only go in order, so 0->1->2->3->0
// tank_status initial status can be 0, 1, or 2. We don't star with emptying the
// tank.
Test test = Test();

// the setup function runs once when you press reset or power the board
void setup() {
  pinMode(pump_top_pin, OUTPUT);
  pinMode(pump_bottom_pin, OUTPUT);
  pinMode(led_error_pin, OUTPUT);
  pinMode(button_pin, INPUT);
  pinMode(water_sensor_top_pin, INPUT);
  pinMode(water_sensor_bottom_pin, INPUT);

  // Set pumps and error messages to off
  digitalWrite(pump_top_pin, LOW);
  digitalWrite(pump_bottom_pin, LOW);
  digitalWrite(led_error_pin, LOW);

  // Check tank status
  if (digitalRead(water_sensor_bottom_pin) == LOW) {
    tank_status = 0; // tank is empty
  } else if (digitalRead(water_sensor_top_pin) == HIGH) {
    tank_status = 2; // tank is full
  } else {
    tank_status = 1; // tank is filling
  } // tank_status 3, emptying is not an option because we want to start
    // watering the garden with a full tank.
}

// the loop function runs over and over again forever
void loop() {
  int water_sensor_bottom = digitalRead(water_sensor_bottom_pin);
  int water_sensor_top = digitalRead(water_sensor_top_pin);
  
  switch (tank_status) {
  case 0: // tank is empty
    digitalWrite(pump_top_pin, LOW);
    digitalWrite(pump_bottom_pin, LOW);
    break;
  case 1: // tank is filling
    digitalWrite(pump_top_pin, HIGH);
    digitalWrite(pump_bottom_pin, LOW);
    break;
  case 2: // tank is full
    digitalWrite(pump_top_pin, LOW);
    digitalWrite(pump_bottom_pin, LOW);
    break;
  case 3: // tank is emptying
    digitalWrite(pump_top_pin, HIGH);
    digitalWrite(pump_bottom_pin, HIGH);
    break;
  default: // unknown tank status
    // Set tank to safest mode, the empty mode
    tank_status = 0;
    break;
  }

  delay(1000);
}
