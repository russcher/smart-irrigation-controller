#ifndef TEST_HPP
#define TEST_HPP

class Test {
public:
    Test();
    ~Test();
    bool enableMotor1();
    bool disableMotor1();

private:
    bool status_motor_1 = false;
};

#endif /* TEST_HPP */